﻿using System;
using System.Collections.Generic;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace DocxLib
{
    public class DocxDocument
    {
        public DocxDocument()
        {
            Document = new Document {Body = new Body()};
            Paragraphs = new List<Paragraph>();
        }

        public Document Document { get; set; }
        public List<Paragraph> Paragraphs { get; set; }

        public void AddH3Text(string text)
        {
            var para = new Paragraph();

            para.AppendText(text);
            para.AppendLineBreak();
            Paragraphs.Add(para);
        }

        /// <summary>
        ///     1 replace full text character with half text character
        ///     2 add text with subscript and superscript
        /// </summary>
        /// <param name="text"></param>
        /// <param name="para"></param>
        public void AppendText(string text, Paragraph para)
        {
            text = text.Normalize(NormalizationForm.FormKC); // fullwidth text to halfwidth text

            //int start = 0;
            //int subIndex = text.IndexOf("<sub>", StringComparison.CurrentCultureIgnoreCase);
            //int superIndex = text.IndexOf("<sup>", StringComparison.CurrentCultureIgnoreCase);
            //int index = superIndex > subIndex ? subIndex : superIndex; // smaller one
            //while (subIndex >= 0)
            //{
            //    var beforeSub = text.Substring(start, subIndex);
            //    para.AppendText(beforeSub);

            //    var subEndIndex = text.IndexOf("</sub>", StringComparison.CurrentCultureIgnoreCase);
            //    var subText = text.Substring(subIndex + 5, (subEndIndex - subIndex - 5));
            //    para.AppendSubScript(subText);
            //    text = text.Substring(subEndIndex + 6);
            //    subIndex = text.IndexOf("<sub>", StringComparison.CurrentCultureIgnoreCase);
            //}

            para.AppendText(text);
        }

        public void AddParagraphText(string text)
        {
            text = text.Normalize(NormalizationForm.FormKC);
            var para = new Paragraph();
            para.AppendText(text);
            para.AppendLineBreak();
            Paragraphs.Add(para);
        }

        public void AddTitleText(string text)
        {
            text = text.Normalize(NormalizationForm.FormKC);
            var para = new Paragraph
            {
                ParagraphProperties = new ParagraphProperties
                {
                    Justification = new Justification {Val = JustificationValues.Center}
                }
            };
            para.AppendText(text);
            para.AppendLineBreak();
            Paragraphs.Add(para);
        }

        public void Save(string fullName)
        {
            using (var doc = WordprocessingDocument.Create(fullName, WordprocessingDocumentType.Document))
            {
                var mainPart = doc.AddMainDocumentPart();
                mainPart.Document = Document;
                Document.Body.Append(Paragraphs);
            }
        }
    }

    public static class Extension
    {
        public static void AppendText(this Paragraph para, string text)
        {
            var run = new Run { RunProperties = GetRunFormat() };
            run.Append(new Text(text));
            para.Append(run);
        }

        public static void AppendSubScript(this Paragraph para, string text)
        {
            var run = new Run { RunProperties = GetRunFormat() };
            var props = new RunProperties();
            var vAlignment = new VerticalTextAlignment { Val = VerticalPositionValues.Subscript };
            props.Append(vAlignment);
            run.Append(props);
            run.Append(new Text(text));
            para.Append(run);
        }

        public static void AppendSuperScript(this Paragraph para, string text)
        {
            var run = new Run { RunProperties = GetRunFormat() };
            var props = new RunProperties();
            var vAlignment = new VerticalTextAlignment { Val = VerticalPositionValues.Superscript };
            props.Append(vAlignment);
            run.Append(props);
            run.Append(new Text(text));
            para.Append(run);
        }

        public static void AppendLineBreak(this Paragraph para)
        {
            var lineBreak = new Run(new Text(Environment.NewLine));
            para.Append(lineBreak);
        }

        private static RunProperties GetRunFormat()
        {
            var properties = new RunProperties
            {
                RunFonts = new RunFonts { Ascii = "MS Gothic", EastAsia = "MS Gothic" },
                FontSize = new FontSize { Val = "26" }
            };
            return properties;
        }
    }
}