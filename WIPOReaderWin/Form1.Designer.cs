﻿namespace WIPOReaderWin
{
    partial class WipoReaderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WipoReaderForm));
            this.KeywordTxt = new System.Windows.Forms.TextBox();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.ConvertBtn = new System.Windows.Forms.Button();
            this._RefreshBtn = new System.Windows.Forms.Button();
            this._AbstractBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // KeywordTxt
            // 
            this.KeywordTxt.Location = new System.Drawing.Point(148, 1);
            this.KeywordTxt.Name = "KeywordTxt";
            this.KeywordTxt.Size = new System.Drawing.Size(110, 20);
            this.KeywordTxt.TabIndex = 1;
            this.KeywordTxt.Text = "WO2014162771";
            // 
            // SearchBtn
            // 
            this.SearchBtn.Location = new System.Drawing.Point(292, 1);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(110, 23);
            this.SearchBtn.TabIndex = 2;
            this.SearchBtn.Text = "Search";
            this.SearchBtn.UseVisualStyleBackColor = true;
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // webBrowser
            // 
            this.webBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser.Location = new System.Drawing.Point(1, 27);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(762, 397);
            this.webBrowser.TabIndex = 3;
            // 
            // ConvertBtn
            // 
            this.ConvertBtn.Enabled = false;
            this.ConvertBtn.Location = new System.Drawing.Point(548, 1);
            this.ConvertBtn.Name = "ConvertBtn";
            this.ConvertBtn.Size = new System.Drawing.Size(110, 23);
            this.ConvertBtn.TabIndex = 4;
            this.ConvertBtn.Text = "Convert to Doc";
            this.ConvertBtn.UseVisualStyleBackColor = true;
            this.ConvertBtn.Click += new System.EventHandler(this.ConvertBtn_Click);
            // 
            // _RefreshBtn
            // 
            this._RefreshBtn.Enabled = false;
            this._RefreshBtn.Location = new System.Drawing.Point(13, 1);
            this._RefreshBtn.Name = "_RefreshBtn";
            this._RefreshBtn.Size = new System.Drawing.Size(110, 23);
            this._RefreshBtn.TabIndex = 5;
            this._RefreshBtn.Text = "Refresh";
            this._RefreshBtn.UseVisualStyleBackColor = true;
            this._RefreshBtn.Click += new System.EventHandler(this.RefreshBtn_Click);
            // 
            // _AbstractBtn
            // 
            this._AbstractBtn.Location = new System.Drawing.Point(420, 1);
            this._AbstractBtn.Name = "_AbstractBtn";
            this._AbstractBtn.Size = new System.Drawing.Size(110, 23);
            this._AbstractBtn.TabIndex = 6;
            this._AbstractBtn.Text = "Abstract";
            this._AbstractBtn.UseVisualStyleBackColor = true;
            this._AbstractBtn.Click += new System.EventHandler(this._AbstractBtn_Click);
            // 
            // WipoReaderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 424);
            this.Controls.Add(this._AbstractBtn);
            this.Controls.Add(this._RefreshBtn);
            this.Controls.Add(this.ConvertBtn);
            this.Controls.Add(this.webBrowser);
            this.Controls.Add(this.SearchBtn);
            this.Controls.Add(this.KeywordTxt);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WipoReaderForm";
            this.Text = "WIPO Reader";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox KeywordTxt;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.Button ConvertBtn;
        private System.Windows.Forms.Button _RefreshBtn;
        private System.Windows.Forms.Button _AbstractBtn;
    }
}

