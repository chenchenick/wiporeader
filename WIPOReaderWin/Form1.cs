﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DocxLib;
using HtmlAgilityPack;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace WIPOReaderWin
{
    public partial class WipoReaderForm : Form
    {
        private readonly string _SearchUrl = "https://patentscope.wipo.int/search";
        private DocxDocument _document = new DocxDocument();

        public WipoReaderForm()
        {
            InitializeComponent();
            webBrowser.ScriptErrorsSuppressed = true; // hide script error
            webBrowser.Navigate(_SearchUrl);
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            if (webBrowser.ReadyState != WebBrowserReadyState.Complete)
                return;

            var pctNum = KeywordTxt.Text.Replace("/", string.Empty);
            if (webBrowser.Document != null)
            {
                var webPctNum = webBrowser.Document.GetElementById("simpleSearchSearchForm:fpSearch");
                var webSearchCmd = webBrowser.Document.GetElementById("simpleSearchSearchForm:commandSimpleFPSearch");
                if (webPctNum != null)
                    webPctNum.InnerText = pctNum;
                else
                {
                    MessageBox.Show("Cannot find search input textbox.");
                    return;
                }
                if (webSearchCmd != null)
                    webSearchCmd.InvokeMember("click");
                else
                {
                    MessageBox.Show("Cannot find search button.");
                    return;
                }
            }
            else
            {
                MessageBox.Show("Web browser critical error. Please restart program.");
                return;
            }

            SearchBtn.Enabled = false;
            _AbstractBtn.Enabled = true;
            ConvertBtn.Enabled = false;
            _RefreshBtn.Enabled = true;
        }

        private void RefreshBtn_Click(object sender, EventArgs e)
        {
            webBrowser.Navigate(_SearchUrl);
            _document = new DocxDocument();
            SearchBtn.Enabled = true;
            _AbstractBtn.Enabled = false;
            ConvertBtn.Enabled = false;
            _RefreshBtn.Enabled = false;
        }

        private void _AbstractBtn_Click(object sender, EventArgs e)
        {
            var abstractText = GetAbstractionText();
            if (!string.IsNullOrEmpty(abstractText))
            {
                AppendBiblioText(abstractText);
                SearchBtn.Enabled = false;
                _AbstractBtn.Enabled = false;
                ConvertBtn.Enabled = true;
                _RefreshBtn.Enabled = true;
            }
        }

        private void ConvertBtn_Click(object sender, EventArgs e)
        {
            AppendParagraphTexts();

            var dialog = new SaveFileDialog
            {
                InitialDirectory = "%USERPROFILE%",
                DefaultExt = "docx",
                RestoreDirectory = true
            };
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                _document.Save(dialog.FileName);
                Process.Start(dialog.FileName);
                // frozen all
                SearchBtn.Enabled = false;
                ConvertBtn.Enabled = false;
                _AbstractBtn.Enabled = false;
                _RefreshBtn.Enabled = true;
            }
        }

        private void AppendBiblioText(string abstractText)
        {
            var titleText = GetTitleText();
            _document.AddTitleText(titleText);

            _document.AddH3Text("Abstract");
            _document.AddParagraphText(abstractText.Replace("　", string.Empty));
        }

        private void AppendParagraphTexts()
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load(webBrowser.DocumentStream, Encoding.UTF8);

            var divs = htmlDoc.DocumentNode.Descendants("div");

            // paragraph contents
            var paragraphsInner = from div in divs
                where div.HasAttributes
                      && div.Attributes.Contains("class")
                      && div.Attributes["class"].Value == "pContent"
                select div;

            foreach (var paragraph in paragraphsInner)
            {
                // if parent sibling is header then get header text
                var sibling = paragraph.ParentNode.PreviousSibling;
                while (sibling.Name != "div")
                {
                    if (sibling.Name == "h3")
                        _document.AddH3Text(sibling.InnerText.Trim().Replace("　", string.Empty));
                    sibling = sibling.PreviousSibling;
                }

                // get paragraph text
                var paraHtml = paragraph.InnerHtml.Trim().Replace("　", string.Empty); // full-width space
                var numHtml = paragraph.PreviousSibling.InnerText;
                if (numHtml.Contains("請求項"))
                {
                    // ignore br in 請求項
                    paraHtml = numHtml + paraHtml.Replace("<br>", string.Empty); // 1. 請求項
                    _document.AddParagraphText(paraHtml);
                }
                else
                {
                    // br as line break in other situation
                    var innerParas = Regex.Split(paraHtml, "<br>");
                    foreach (var innerPara in innerParas)
                        _document.AddParagraphText(innerPara);
                }
            }
        }

        private string GetTitleText()
        {
            var result = string.Empty;
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load(webBrowser.DocumentStream, Encoding.UTF8);

            var spans = htmlDoc.DocumentNode.Descendants("span");
            var titleSpan = from span in spans
                where span.HasAttributes
                      && span.Attributes.Contains("class")
                      && span.Attributes["class"].Value == "PCTtitle trans-section"
                      && span.Attributes.Contains("lang")
                      && span.Attributes["lang"].Value == "ja"
                select span;

            var htmlNodes = titleSpan as HtmlNode[] ?? titleSpan.ToArray();
            if (htmlNodes.Any())
                result = htmlNodes.First().InnerText;
            return result;
        }

        private string GetAbstractionText()
        {
            var abstractText = string.Empty;
            var spans = GetBiblioSpans();
            var abstractSpan = from span in spans
                where span.HasAttributes
                      && span.Attributes.Contains("class")
                      && span.Attributes["class"].Value == "PCTabstract trans-section"
                      && span.Attributes.Contains("lang")
                      && span.Attributes["lang"].Value == "ja"
                select span;

            var htmlNodes = abstractSpan as HtmlNode[] ?? abstractSpan.ToArray();
            if (htmlNodes.Any())
                abstractText = htmlNodes[0].InnerText;
            return abstractText;
        }

        private IEnumerable<HtmlNode> GetBiblioSpans()
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load(webBrowser.DocumentStream, Encoding.UTF8);

            var spans = htmlDoc.DocumentNode.Descendants("span");
            return spans;
        }
    }
}