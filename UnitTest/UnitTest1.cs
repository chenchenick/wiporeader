﻿using System;
using NUnit.Framework;
using DocxLib;
using DocumentFormat.OpenXml.Wordprocessing;

namespace UnitTest
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void TestMethod1()
        {
            string text = "This element spe<sub>ci</sub>fies the subscript object sSub";
            DocxDocument d = new DocxDocument();
            d.AppendText(text, new Paragraph());
            Assert.IsNotNull(d);
        }
    }
}
